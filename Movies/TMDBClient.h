//
//  TMDBClient.h
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "HTTPClient.h"

@class Configuration;

FOUNDATION_EXPORT NSString * const TMDBBaseURL;
FOUNDATION_EXPORT NSString * const TMDBClientDidUpdateConfigurationNotification;

@interface TMDBClient : HTTPClient

@property (nonatomic, strong) Configuration *configuration;

- (void)getConfigurationWithCompletion:(void (^)(NSData *config, NSError *error))completion;
- (void)getGenresWithCompletion:(void (^)(NSData *data, NSError *error))completion;
- (void)getPopularMoviesPage:(NSUInteger)page completion:(void (^)(NSData *, NSError *))completion;

- (void)updateConfiguration;

@end

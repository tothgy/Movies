//
//  MovieViewController.swift
//  Movies
//
//  Created by tothgy on 20/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

import UIKit

@objc class MovieViewController: UITableViewController {

    let movie: Movie
    var genreList: GenreList!
    var client: TMDBClient!
    var configuration: Configuration!

    init(movie: Movie) {
        self.movie = movie

        super.init(style: .Plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        let titleCellNib = UINib(nibName: "MovieTableViewCell", bundle: nil)
        let overviewCellNib = UINib(nibName: "MovieOverviewTableViewCell", bundle: nil)

        self.tableView.registerNib(titleCellNib, forCellReuseIdentifier: MovieTableViewCellId)
        self.tableView.registerNib(overviewCellNib, forCellReuseIdentifier: MovieOverviewTableViewCellId)

        self.tableView.estimatedRowHeight = 210
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

    }

    // MARK: UITableViewDataSource

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier(MovieTableViewCellId)!

            self.configureTitleCell(cell as! MovieTableViewCell)

            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier(MovieOverviewTableViewCellId)!

            self.configureOverViewCell(cell as! MovieOverviewTableViewCell)

            return cell
         }
    }

    func configureTitleCell(cell: MovieTableViewCell) {
        let imageWidthPathURL = NSURL(string: ListImageWidth)
        let imageURLPath = imageWidthPathURL?.URLByAppendingPathComponent(self.movie.posterPath)
        let imageURL = NSURL(string: (imageURLPath?.absoluteString)!, relativeToURL: self.configuration.imageBaseURL)

        cell.popularityLabel.text = movie.popularity.stringValue
        cell.titleLabel.text = movie.title
        cell.thumbnailImageView.imageURL = imageURL
    
        cell.genreLabel.text = self.genreList.genreNamesStringFromIds(self.movie.genres)
    }

    func configureOverViewCell(cell: MovieOverviewTableViewCell) {
        cell.overViewLabel.text = self.movie.overview
    }

}

//
//  MovieCell.h
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AsyncImageView;

FOUNDATION_EXPORT NSString * const MovieTableViewCellId;

@interface MovieTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet AsyncImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *popularityLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;

@end

//
//  AppCoordinator.h
//  Movies
//
//  Created by tothgy on 16/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIWindow;

@interface AppCoordinator : NSObject

- (instancetype)initWithWindow:(UIWindow *)window;

- (void)start;

@end

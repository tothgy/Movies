//
//  HTTPClient.h
//  Movies
//
//  Created by tothgy on 16/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTTPClient : NSObject

- (instancetype)initWithBaseURL:(NSURL *)baseURL;

- (void)getPath:(NSString *)path
     parameters:(NSDictionary *)parameters
     completion:(void (^)(NSData *responseData, NSError *error))completion;

- (NSURLSessionConfiguration *)sessionConfiguration;

@end

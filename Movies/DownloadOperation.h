//
//  DownloadOperation.h
//  Movies
//
//  Created by tothgy on 16/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PersistenceController;

@interface DownloadOperation : NSOperation

/**
 INPUT
 */
@property (nonatomic, copy) NSURL *URL;

/**
 OUTPUT
 */
@property (nonatomic, copy) void (^complete)(NSData *csv, NSError *error);

@end

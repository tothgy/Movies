//
//  MovieListViewController.h
//  Movies
//
//  Created by tothgy on 16/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieList.h" // TODO: move movie listdelegate to a separate file

@class MovieList, GenreList, Configuration, MovieListViewController;

/**
 Navigation related protocol
 */
@protocol MovieListViewControllerDelegate <NSObject>

- (void)movieListViewController:(MovieListViewController *)viewController
                 didSelectMovie:(Movie *)movie;

@end

@interface MovieListViewController : UITableViewController <MovieListDelegate>

@property (nonatomic, strong) MovieList *movieList;
@property (nonatomic, strong) GenreList *genreList;
@property (nonatomic, strong) Configuration *configuration;

@property (nonatomic, weak) id<MovieListViewControllerDelegate> delegate;

@end


//
//  HTTPClient.m
//  Movies
//
//  Created by tothgy on 16/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "HTTPClient.h"
#import "DownloadOperation.h"

@interface HTTPClient ()

@property (nonatomic, strong) NSURL *baseURL;
@property (nonatomic, strong) NSOperationQueue *operationQueue;

@end

@implementation HTTPClient

- (instancetype)initWithBaseURL:(NSURL *)baseURL
{
    self = [super init];

    if (self) {
        _baseURL = baseURL;
        _operationQueue = [[NSOperationQueue alloc] init];
    }

    return self;
}

- (void)getPath:(NSString *)path
     parameters:(NSDictionary *)parameters
     completion:(void (^)(NSData *, NSError *))completion
{
    NSURL *URL = [NSURL URLWithString:path relativeToURL:self.baseURL];
    NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:URL
                                                resolvingAgainstBaseURL:YES];

    urlComponents.queryItems = [self buildQueryItems:parameters];

    DownloadOperation *downloadOperation = [[DownloadOperation alloc] init];

    downloadOperation.URL = urlComponents.URL;
    downloadOperation.complete = completion;

    [self.operationQueue addOperation:downloadOperation];
}

- (NSArray *)buildQueryItems:(NSDictionary *)parameters
{
    NSMutableArray *queryItems = [[NSMutableArray alloc] init];

    [parameters enumerateKeysAndObjectsUsingBlock:
        ^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            NSURLQueryItem *queryItem = [NSURLQueryItem queryItemWithName:key value:obj];

            [queryItems addObject:queryItem];
    }];

    return queryItems;
}

- (NSURLSessionConfiguration *)sessionConfiguration
{
    return nil;
}

@end

//
//  AppCoordinator.m
//  Movies
//
//  Created by tothgy on 16/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCoordinator.h"
#import "MovieListViewController.h"
#import "TMDBClient.h"
#import "MovieList.h"
#import "GenreList.h"
#import "Movies-Swift.h"

@interface AppCoordinator () <MovieListViewControllerDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic, strong) TMDBClient *tmdbClient;
@property (nonatomic, strong) MovieList *movieList;
@property (nonatomic, strong) GenreList *genreList;

@end

@implementation AppCoordinator

- (instancetype)initWithWindow:(UIWindow *)window
{
    self = [super init];

    if (self) {
        _window = window;
        _navigationController = [[UINavigationController alloc] init];

        _window.rootViewController = _navigationController;

        _movieList = [[MovieList alloc] init];
        _genreList = [[GenreList alloc] init];
        _tmdbClient = [[TMDBClient alloc] init];

        _genreList.client = _tmdbClient;
        _movieList.client = _tmdbClient;
        _movieList.genreList = _genreList;
    }

    return self;
}

- (void)start
{
    MovieListViewController *viewController = [[MovieListViewController alloc] init];

    viewController.delegate = self;
    viewController.movieList = self.movieList;
    viewController.genreList = self.genreList;
    viewController.configuration = self.tmdbClient.configuration;

    // TODO: move from here
    [self.tmdbClient updateConfiguration];

    [self.navigationController setViewControllers:@[ viewController ] animated:NO];
}

#pragma mark - MovieListViewControllerDelegate

- (void)movieListViewController:(MovieListViewController *)viewController
                 didSelectMovie:(Movie *)movie
{
    MovieViewController *movieViewController = [[MovieViewController alloc] initWithMovie:movie];

    movieViewController.configuration = self.tmdbClient.configuration;
    movieViewController.genreList = self.genreList;

    [self.navigationController pushViewController:movieViewController animated:YES];
}

@end

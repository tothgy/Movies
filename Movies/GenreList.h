//
//  GenreList.h
//  Movies
//
//  Created by tothgy on 20/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TMDBClient, Genre, GenreList;

/**
 A delegate of the Genre list will get callbacks on list updates
 */
@protocol GenreListDelegate <NSObject>

/**
 Callback method on successful list update
 
 This event might be useful for view controllers, so that it is executed in the Main thread
 
 @param genreList the list updated
 */
- (void)genreListFetchDidFinish:(GenreList *)genreList;

@end

/**
 Represents a list of Movie genres from The Movie Db
 */
@interface GenreList : NSObject

@property (nonatomic, strong) TMDBClient *client;
@property (nonatomic, weak) id<GenreListDelegate> delegate;

@property (nonatomic, strong) NSArray *genres;

/**
 Start fetching the list of Movie genres asynchronously
 
 The delegate will be notified when the list is successfully downloaded
 */
- (void)fetchGenres;

/**
 Genre lookup by id

 The list of genres must be fetched first by `fetchGenres`
 
 @param generId: id of the Genre
 */
- (Genre *)genreForId:(NSNumber *)genreId;

- (NSString *)genreNamesStringFromIds:(NSArray *)genreIds;

@end

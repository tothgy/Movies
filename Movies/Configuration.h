//
//  Configuration.h
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString * const ListImageWidth;

/**
 Represents the system wide configuration of The Movie Db
 */
@interface Configuration : NSObject

@property (nonatomic, strong) NSURL *imageBaseURL;

@end

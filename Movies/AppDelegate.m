//
//  AppDelegate.m
//  Movies
//
//  Created by tothgy on 16/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "AppDelegate.h"
#import "AppCoordinator.h"

@interface AppDelegate ()

@property (nonatomic, strong) AppCoordinator *appCoordinator;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UIWindow *window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    AppCoordinator *appCoordinator = [[AppCoordinator alloc] initWithWindow:window];

    [appCoordinator start];

    self.appCoordinator = appCoordinator;
    self.window = window;
    [self.window makeKeyAndVisible];

    return YES;
}

@end

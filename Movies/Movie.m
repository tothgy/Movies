//
//  Movie.m
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "Movie.h"
#import "Genre.h"
#import "Motis.h"

@implementation Movie

+ (NSDictionary *)mts_mapping
{
    return @{
        @"id" : mts_key(movieId),
        @"title" : mts_key(title),
        @"popularity" : mts_key(popularity),
        @"poster_path" : mts_key(posterPath),
        @"release_date" : mts_key(releaseDate),
        @"genre_ids" : mts_key(genres),
        @"overview" : mts_key(overview)
    };
}

+ (NSDateFormatter *)mts_validationDateFormatter
{
    static NSDateFormatter *dateFormatter;

    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        dateFormatter.dateFormat = @"YYYY-MM-dd";
    }

    return dateFormatter;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ - %@ (%@)", self.movieId, self.title, self.releaseDate];
}

@end

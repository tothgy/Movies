//
//  DownloadOperation.m
//  Movies
//
//  Created by tothgy on 16/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "DownloadOperation.h"

static NSString * const kDefaultErrorDomain = @"tothgy.INM";
static const NSInteger kDefaultErrorCode = -1;

@interface DownloadOperation ()

@property (nonatomic, assign, getter=isExecuting) BOOL executing;
@property (nonatomic, assign, getter=isFinished) BOOL finished;

@property (nonatomic, strong) NSURLSession *session;

@end

@implementation DownloadOperation

@synthesize executing = _executing;
@synthesize finished = _finished;

#pragma mark - NSOperation

- (instancetype)init
{
    self = [super init];

    if (self) {
        _session = [NSURLSession sessionWithConfiguration:[self sessionConfiguration]
                                                 delegate:nil
                                            delegateQueue:nil];
    }

    return self;
}

- (void)start
{
    self.executing = YES;

    [self download];
}

- (BOOL)isAsynchronous
{
    return YES;
}

- (BOOL)isConcurrent
{
    return YES;
}

- (void)setExecuting:(BOOL)executing
{
    [self willChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
    _executing = executing;
    [self didChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
}

- (void)setFinished:(BOOL)finished
{
    [self willChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
    _finished = finished;
    [self didChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
}

#pragma mark -

- (NSURLSessionConfiguration *)sessionConfiguration
{
    NSURLSessionConfiguration *sessionConfiguration =
        [NSURLSessionConfiguration defaultSessionConfiguration];

    sessionConfiguration.HTTPAdditionalHeaders = @{
        @"Content-Type" : @"application/json"
    };

    return sessionConfiguration;
}

- (void)download
{
    NSURLSessionTask *task =
        [self.session dataTaskWithURL:self.URL completionHandler:
            ^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                [self handleResponse:response data:data error:error];
            }];

    [task resume];
}

- (void)handleResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *)error
{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;

    // TODO: handle redirection
    if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300) {
        [self completeOperationWithResult:data error:nil];
    } else if (error) {
        [self completeOperationWithResult:nil error:error];
    } else {
        NSError *downloadError = [self errorFromData:data];

        [self completeOperationWithResult:nil error:downloadError];
    }
}

- (NSError *)errorFromData:(NSData *)data
{
    NSString *errorString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSDictionary *errorInfo = @{
        NSLocalizedDescriptionKey : errorString
    };

    NSError *error =
        [NSError errorWithDomain:kDefaultErrorDomain
                            code:kDefaultErrorCode
                        userInfo:errorInfo];
    
    return error;
}

- (void)completeOperationWithResult:(NSData *)data error:(NSError *)error
{
    self.complete(data, error);
    
    self.executing = NO;
    self.finished = YES;
}

@end

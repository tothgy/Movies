//
//  Genre.m
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "Genre.h"
#import "Movie.h"
#import "Motis.h"

@implementation Genre

+ (NSDictionary *)mts_mapping
{
    return @{
        @"id" : mts_key(genreId),
        @"name" : mts_key(name)
    };
}

@end

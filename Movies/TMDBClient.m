//
//  TMDBClient.m
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "TMDBClient.h"
#import "Configuration.h"
#import "Motis.h"

NSString * const TMDBBaseURL = @"https://api.themoviedb.org/";
NSString * const TMDBClientDidUpdateConfigurationNotification =
    @"tothgy.INM.TMDBClientDidUpdateConfigurationNotification";

static NSString * const API_KEY = @"ba67f74ba49baf401ddfd4d00a153eb1";

static NSString * const RequestApiKeyKey = @"api_key";
static NSString * const RequestPageKey = @"page";

static NSString * const ConfigurationPath = @"3/configuration";
static NSString * const GenresPath = @"3/genre/movie/list";
static NSString * const PopularMoviesPath = @"3/movie/popular";

@implementation TMDBClient

- (instancetype)init
{
    self = [super initWithBaseURL:[NSURL URLWithString:TMDBBaseURL]];

    if (self) {
        _configuration = [[Configuration alloc] init];
    }

    return self;
}

- (void)getConfigurationWithCompletion:(void (^)(NSData *, NSError *))completion
{
    [super getPath:ConfigurationPath
        parameters:@{
            RequestApiKeyKey : API_KEY
        }
        completion:completion];
}

- (void)getGenresWithCompletion:(void (^)(NSData *, NSError *))completion
{
    [super getPath:GenresPath
        parameters:@{
            RequestApiKeyKey : API_KEY
        }
        completion:completion];
}

- (void)getPopularMoviesPage:(NSUInteger)page completion:(void (^)(NSData *, NSError *))completion
{
    [super getPath:PopularMoviesPath
        parameters:@{
            RequestApiKeyKey : API_KEY,
            RequestPageKey : @(page).stringValue
        }
        completion:completion];
}

- (void)updateConfiguration
{
    [super getPath:ConfigurationPath
        parameters:@{
            RequestApiKeyKey : API_KEY
        }
        completion:^(NSData *responseData, NSError *error) {
            NSDictionary *configJSON =
                [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];

            [self.configuration mts_setValuesForKeysWithDictionary:configJSON];
            [self notifyObserversOfUpdateFinish];
        }];
}

#pragma mark - HTTPClient

- (NSURLSessionConfiguration *)sessionConfiguration
{
    return [NSURLSessionConfiguration defaultSessionConfiguration];
}

#pragma mark - 

- (void)notifyObserversOfUpdateFinish
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter]
            postNotificationName:TMDBClientDidUpdateConfigurationNotification
                          object:self
                        userInfo:nil];
    });
}

@end

//
//  GenreList.m
//  Movies
//
//  Created by tothgy on 20/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "GenreList.h"
#import "TMDBClient.h"
#import "Genre.h"
#import "Motis.h"

@interface GenreList ()

@property (nonatomic, strong) NSDictionary *genreLookupDictionary;

@end

@implementation GenreList

- (void)fetchGenres
{
    [self.client getGenresWithCompletion:^(NSData *data, NSError *error) {
        NSDictionary *genresJSON =
            [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

        NSMutableArray *genres = [[NSMutableArray alloc] init];

        for (NSDictionary *genreJSON in genresJSON[@"genres"]) {
            Genre *genre = [[Genre alloc] init];

            [genre mts_setValuesForKeysWithDictionary:genreJSON];

            [genres addObject:genre];
        }

        self.genres = [genres copy];

        [self notifyDelegateOfFetchFinish];
    }];
}

- (Genre *)genreForId:(NSNumber *)genreId
{
    return self.genreLookupDictionary[genreId];
}

- (NSString *)genreNamesStringFromIds:(NSArray *)genreIds
{
    NSMutableArray *genres = [[NSMutableArray alloc] initWithCapacity:genreIds.count];

    for (NSNumber *genreId in genreIds) {
        Genre *genre = [self.genreLookupDictionary objectForKey:genreId];

        if (genre) {
            [genres addObject:genre];
        }
    }

    return [[genres valueForKeyPath:@"@unionOfObjects.name"] componentsJoinedByString:@", "];
}

- (void)setGenres:(NSArray *)genres
{
    NSMutableDictionary *genreLookupDictionary = [[NSMutableDictionary alloc] init];

    for (Genre *genre in genres) {
        [genreLookupDictionary setObject:genre forKey:genre.genreId];
    }

    _genres = genres;
    _genreLookupDictionary = [genreLookupDictionary copy];
}

- (void)notifyDelegateOfFetchFinish
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate genreListFetchDidFinish:self];
    });
}

@end

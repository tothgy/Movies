//
//  MovieOverviewTableViewCell.swift
//  Movies
//
//  Created by tothgy on 20/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

import UIKit

let MovieOverviewTableViewCellId: String = "MovieOverviewTableViewCellId"

class MovieOverviewTableViewCell: UITableViewCell {

    @IBOutlet weak var overViewLabel: UILabel!
    
}

//
//  MovieList.m
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "MovieList.h"
#import "TMDBClient.h"
#import "AWPagedArray.h"
#import "Movie.h"
#import "GenreList.h"
#import "Genre.h"
#import "Motis.h"

@interface MovieList () <AWPagedArrayDelegate, GenreListDelegate>

@property (nonatomic, strong) AWPagedArray *movieList;
@property (nonatomic, assign) NSUInteger currentPage;
@property (nonatomic, strong) NSMutableIndexSet *dataLoadingOperations;

@end

@implementation MovieList

- (instancetype)init
{
    self = [super init];

    if (self) {
        _currentPage = 1;
        _dataLoadingOperations = [[NSMutableIndexSet alloc] init];
    }

    return self;
}

#pragma mark - Public

- (void)fetchMovies
{
    self.genreList.delegate = self;

    // list of genres are required to properly present movie details
    // on successful genre list download the movie list download is kicked off
    [self.genreList fetchGenres];
}

- (NSUInteger)numberOfMovies
{
    return ((NSArray *)self.movieList).count;
}

- (Movie *)movieAtIndex:(NSUInteger)index
{
    if (index < ((NSArray *)self.movieList).count) {
        return ((NSArray *)self.movieList)[index];
    } else {
        return nil;
    }
}

#pragma mark - AWPagedArrayDelegate

- (void)pagedArray:(AWPagedArray *)pagedArray
   willAccessIndex:(NSUInteger)index
      returnObject:(__autoreleasing id *)returnObject
{
    if ([*returnObject isKindOfClass:[NSNull class]]) {
        NSUInteger page = [pagedArray pageForIndex:index];

        [self loadDataForPageIfNeeded:page];
    }
}

#pragma mark - GenreListDelegate

- (void)genreListFetchDidFinish:(GenreList *)genreList
{
    [self initialMoviePageFetch];
}

#pragma mark - 

- (void)initialMoviePageFetch
{
    [self.dataLoadingOperations addIndex:self.currentPage];

    [self.client getPopularMoviesPage:self.currentPage completion:^(NSData *data, NSError *error) {
        if (!data) {
            [self notifyDelegateWithFetchError:error];
        }

        NSDictionary *responseJSON =
            [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];

        NSUInteger totalPages = [responseJSON[@"total_pages"] unsignedIntegerValue];

        self.movieList =
            [[AWPagedArray alloc] initWithCount:MIN(50, totalPages - 1)
                                 objectsPerPage:20
                               initialPageIndex:self.currentPage];

        self.movieList.delegate = self;

        NSArray *movies = [self parseResponse:responseJSON[@"results"]];

        [self.movieList setObjects:movies forPage:self.currentPage];

        [self notifyDelegateForInitFinished];

        NSIndexSet *indexSet = [self.movieList indexSetForPage:self.currentPage];

        [self notifyDelegateWithUpdatedIndexes:indexSet];

        [self.dataLoadingOperations removeIndex:self.currentPage];
        self.currentPage++;
    }];
}

- (void)loadDataForPage:(NSUInteger)page
{
    [self.dataLoadingOperations addIndex:page];

    [self.client getPopularMoviesPage:page completion:^(NSData *responseData, NSError *error) {
        NSDictionary *responseJSON =
            [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];

        NSArray *movies = [self parseResponse:responseJSON[@"results"]];

        [self.movieList setObjects:movies forPage:page];

        NSIndexSet *indexSet = [self.movieList indexSetForPage:page];

        [self.dataLoadingOperations removeIndex:page];
        [self notifyDelegateWithUpdatedIndexes:indexSet];
    }];
}

- (void)loadDataForPageIfNeeded:(NSUInteger)page
{
    BOOL isPageExists = self.movieList.pages[@(page)] != nil;
    BOOL isPageDownloadInProgress = [self.dataLoadingOperations containsIndex:page];

    if (!isPageExists && !isPageDownloadInProgress) {
        [self loadDataForPage:page];
    }
}

- (NSArray *)parseResponse:(NSArray *)moviesJSON
{
    NSMutableArray *movies = [[NSMutableArray alloc] init];

    for (NSDictionary *movieJSON in moviesJSON) {
        Movie *movie = [[Movie alloc] init];

        [movie mts_setValuesForKeysWithDictionary:movieJSON];
        [movies addObject:movie];
    }

    return movies;
}

- (void)notifyDelegateForInitFinished
{
    dispatch_sync(dispatch_get_main_queue(), ^{
        [self.delegate movieListDidInit:self];
    });
}

- (void)notifyDelegateWithUpdatedIndexes:(NSIndexSet *)indexSet
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate movieList:self didLoadDataAtIndexes:indexSet];
    });
}

- (void)notifyDelegateWithFetchError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate movieList:self didFailFetchWithError:error];
    });
}

@end

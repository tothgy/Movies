//
//  MovieList.h
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TMDBClient, MovieList, Movie, GenreList;

typedef NS_ENUM(NSInteger, MovieListChangeType) {
    MovieListChangeTypeInsert
};

@protocol MovieListDelegate <NSObject>

- (void)movieListDidInit:(MovieList *)movieList;

- (void)movieList:(MovieList *)movieList didLoadDataAtIndexes:(NSIndexSet *)indexes;

- (void)movieList:(MovieList *)movieList didFailFetchWithError:(NSError *)error;

@end

@interface MovieList : NSObject

@property (nonatomic, strong) TMDBClient *client;
@property (nonatomic, strong) GenreList *genreList;
@property (nonatomic, weak) id<MovieListDelegate> delegate;

- (void)fetchMovies;

- (NSUInteger)numberOfMovies;
- (Movie *)movieAtIndex:(NSUInteger)index;

@end

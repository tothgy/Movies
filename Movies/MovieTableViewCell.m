//
//  MovieCell.m
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "MovieTableViewCell.h"
#import "AsyncImageView.h"

NSString * const MovieTableViewCellId = @"MovieTableViewCellId";

@implementation MovieTableViewCell

@end

//
//  MovieListViewController.m
//  Movies
//
//  Created by tothgy on 16/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "MovieListViewController.h"
#import "Movie.h"
#import "Configuration.h"
#import "MovieTableViewCell.h"
#import "AsyncImageView.h"
#import "GenreList.h"
#import "Genre.h"

static const CGFloat kDefaultCellHeight = 213.0f;

@interface MovieListViewController ()

@end

@implementation MovieListViewController

- (instancetype)init
{
    return [super initWithStyle:UITableViewStylePlain];
}

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    UINib *movieCellNib = [UINib nibWithNibName:@"MovieTableViewCell" bundle:nil];

    [self.tableView registerNib:movieCellNib forCellReuseIdentifier:MovieTableViewCellId];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    self.movieList.delegate = self;

    if ([self.movieList numberOfMovies] == 0) {
        // fetch the list if it has not loaded yet
        // at this stage there is no need to refresh the list when it's already loaded
        [self.movieList fetchMovies];
    }
}

#pragma mark - MovieListDelegate

- (void)movieListDidInit:(MovieList *)movieList
{
    [self.tableView reloadData];
}

- (void)movieList:(MovieList *)movieList didLoadDataAtIndexes:(NSIndexSet *)indexes
{
    NSMutableArray *indexPathsToReload = [NSMutableArray array];

    [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:idx inSection:0];

        if ([self.tableView.indexPathsForVisibleRows containsObject:indexPath]) {
            [indexPathsToReload addObject:indexPath];
        }
    }];

    if (indexPathsToReload.count > 0) {
        [self.tableView reloadRowsAtIndexPaths:indexPathsToReload
                              withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)movieList:(MovieList *)movieList didFailFetchWithError:(NSError *)error
{
    // TODO: show error
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.movieList numberOfMovies];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Movie *movie = [self.movieList movieAtIndex:indexPath.row];
    MovieTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MovieTableViewCellId];

    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.thumbnailImageView];
    cell.thumbnailImageView.image = nil;

    if ([movie isEqual:[NSNull null]]) {
        cell.titleLabel.text = nil;
        cell.popularityLabel.text = nil;
        cell.genreLabel.text = nil;
    } else {
        NSString *imageURLPath = [ListImageWidth stringByAppendingPathComponent:movie.posterPath];
        NSURL *imageURL =
            [NSURL URLWithString:imageURLPath relativeToURL:self.configuration.imageBaseURL];

        cell.popularityLabel.text = movie.popularity.stringValue;
        cell.titleLabel.text = movie.title;
        cell.thumbnailImageView.imageURL = imageURL;

        cell.genreLabel.text = [self.genreList genreNamesStringFromIds:movie.genres];
    }

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Movie *movie = [self.movieList movieAtIndex:indexPath.row];

    [self.delegate movieListViewController:self didSelectMovie:movie];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kDefaultCellHeight;
}

@end

//
//  Genre.h
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Movie;

@interface Genre : NSObject

@property (nullable, nonatomic, retain) NSNumber *genreId;
@property (nullable, nonatomic, retain) NSString *name;

@end

//
//  Movie.h
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Genre;

@interface Movie : NSObject

@property (nonatomic, strong) NSNumber *movieId;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSDate *releaseDate;
@property (nonatomic, strong) NSNumber *popularity;
@property (nonatomic, strong) NSString *posterPath;
@property (nonatomic, strong) NSArray *genres;
@property (nonatomic, strong) NSString *overview;

@end

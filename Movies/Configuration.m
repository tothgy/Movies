//
//  Configuration.m
//  Movies
//
//  Created by tothgy on 17/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import "Configuration.h"
#import "Motis.h"

NSString * const ListImageWidth = @"w154"; // TODO: get the value from the Configuration JSON

@implementation Configuration

+ (NSDictionary *)mts_mapping
{
    return @{
        @"images.secure_base_url" : mts_key(imageBaseURL)
    };
}

@end

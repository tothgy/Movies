//
//  GenreListTests.m
//  MoviesTests
//
//  Created by tothgy on 16/08/16.
//  Copyright © 2016 Gyorgy Toth. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "TMDBClient.h"
#import "GenreList.h"
#import "Genre.h"

@interface GenreListTests : XCTestCase

@property (nonatomic, strong) TMDBClient *clientMock;
@property (nonatomic, strong) GenreList *genreList;

@end

@implementation GenreListTests

- (void)setUp
{
    [super setUp];

    self.clientMock = OCMClassMock([TMDBClient class]);
    self.genreList = [[GenreList alloc] init];
}

- (void)testSuccessfulGenreListFetch
{
    XCTestExpectation *expectation = [self expectationWithDescription:@""];
    id<GenreListDelegate> delegate = OCMProtocolMock(@protocol(GenreListDelegate));

    [self stubClientMockWithSuccessfulResult:nil];

    OCMStub([delegate genreListFetchDidFinish:OCMOCK_ANY]).andDo(^(NSInvocation *invocation) {
        XCTAssertEqual(self.genreList.genres.count, 20);
        // TODO: comparision of elements

        [expectation fulfill];
    });

    self.genreList.client = self.clientMock;
    self.genreList.delegate = delegate;

    [self.genreList fetchGenres];

    [self waitForExpectationsWithTimeout:0.1 handler:nil];
}

- (void)testGenreNamesStringGeneration
{
    self.genreList.genres = [self sampleGenres];

    NSArray *genreIds = @[ @(1), @(2) ];
    NSString *genreNamesString = [self.genreList genreNamesStringFromIds:genreIds];

    NSString *expectedString = @"genre 1, genre 2";

    XCTAssertEqualObjects(genreNamesString, expectedString);
}

- (void)testGenreLookup
{
    self.genreList.genres = [self sampleGenres];

    Genre *genre = [self.genreList genreForId:@(1)];

    XCTAssertEqualObjects(genre.genreId, @(1));
    XCTAssertEqualObjects(genre.name, @"genre 1");
}

#pragma mark - 

- (void)stubClientMockWithSuccessfulResult:(id)clientMock
{
    OCMStub([self.clientMock getGenresWithCompletion:OCMOCK_ANY]).andDo(^(NSInvocation *invocation) {
        void (^completionBlock)(NSData *data, NSError *error);

        [invocation getArgument:&completionBlock atIndex:2];

        NSString *jsonPath =
            [[NSBundle bundleForClass:[self class]] pathForResource:@"genre_list" ofType:@"json"];

        NSData *data  = [NSData dataWithContentsOfFile:jsonPath];

        completionBlock(data, nil);
    });
}

- (NSArray *)sampleGenres
{
    Genre *genre1 = [[Genre alloc] init];
    Genre *genre2 = [[Genre alloc] init];

    genre1.genreId = @(1);
    genre1.name = @"genre 1";

    genre2.genreId = @(2);
    genre2.name = @"genre 2";

    return @[ genre1, genre2 ];
}

@end

## Movies
A simple iOS app to display a list of movies using themoviedb.org API and to allow the user to get more details about each movie she clicks onto.

## To run

Open the project file and run.

## Features

* Built around the Cocoa MVC pattern.
* An **Application Coordinator** is responsible for the navigation logic and dependency injection, so that it frees the view controllers of the extra responsibility of navigation. This way the view controllers remain isolated, self-contained as well as more reuseable, while the **Application Coordinator** can become a server of ad-hoc or structured application screen requests - deep links, and serves as dependency injection environment.
* Fluent scrolling in the table view with the paging concealed.
* TMDb.org related data - link, API key, endpoints, centralised in the TDMBClient class
* Free of third party dependency manager.
* Asynchronous data parsing and image loading, with image caching.

## Notes

* The user interface and layout is a bit rough at this stage but the purpose of the project is more to demonstrate an extendable and testable application architecture.
* To fully exploit the benefits of fluent scrolling in the table view, the row height is fixed.
* The original idea was to implement the views and view controllers in Swift, part of them remains to be rewritten due to lack of time.

## TODO

* Pull to refresh controller
* Improve interface design
* Extend test coverage

